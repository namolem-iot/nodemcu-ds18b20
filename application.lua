t = require("ds18b20")
t:readTemp(readout, tempPin)

function readout(temp)
  for addr, temp in pairs(temp) do
    print(string.format("Sensor %s: %s 'C", encoder.toHex(addr), temp))
    sendData(temp)
  end

  -- Module can be released when it is no longer needed
  t = nil
  package.loaded["ds18b20"]=nil
end

function goToSleep()
    print("Going to deep sleep for "..(time_between_sensor_readings/1000).." seconds")
    node.dsleep(time_between_sensor_readings*1000)
end

function sendData(temp)
  print("Posting data...")
  body = "{ \"sensorId\" : \"".. sensorId .."\", \"value\" : ".. temp .."  }"
  print("Body: " .. body)
  http.post("http://192.168.11.9:3000/values",
            " HTTP/1.1\r\n" ..
            "Host: 192.168.11.9:3000\r\n"..
            "Content-Type: application/json\r\n",
            body,
            httpCallback
          )
end

function httpCallback(code, data)
  if (code < 0) then
    print("HTTP request failed")
  else
    print(code,data)
  end
  goToSleep()
end
