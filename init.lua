tempPin = 1 -- gpio0 = 3, gpio2 = 4
STATUS_TEMP_UNAVAILABLE = -999
lastTemperature = STATUS_TEMP_UNAVAILABLE
time_between_sensor_readings = 60000
cfg = {}
dofile("settings.lua")
thingspeak_channel_api_write_key=cfg.ThingSpeakApiKey

client_ip="192.168.11.50"
client_netmask="255.255.255.0"
client_gateway="192.168.11.1"


sensorId = cfg.sensorId

function startup()
  if file.open("init.lua") == nil then
    print("init.lua deleted or renamed")
  else
    print("Running")
    file.close("init.lua")
    dofile("application.lua")
  end
end

wifi_connect_event = function(T)
  print("Connection to AP(" .. T.SSID..") established!")
  print("Waiting for IP address..")
  if disconnect_ct ~= nil then disconnect_ct = nul end
end

wifi_got_ip_event = function(T)
  print("Wifi connection is ready! IP address is :".. T.IP)
  print("Startup will resume momentarily, you have 3 seconds to abort.")
  print("Waiting...")
  tmr.create():alarm(3000, tmr.ALARM_SINGLE, startup)
end

wifi_disconnect_event = function(T)
  if T.reason == wifi_eventmon.reason.ASSOC_LEAVE then
    --the station has disassociated from a previously connected AP
    return
  end
  local total_tries = 75
  print("\nWiFi connection to AP(" .. T.SSID .. ") has failed!")

  for key,val in pairs(wifi.eventmon.reason) do
    if val == T.reason then
      print("Disconnect reason: ".. val.."("..key..")")
      break
    end
  end

  if disconnect_ct == nil then
    disconnect_ct = 1
  else
    disconnect_ct = disconnect_ct+1
  end
  if disconnect_ct < total_tries then
    print("Retrying connection... (attempt "..(disconnect_ct+1).." of "..total_tries..")")
  else
    wifi.sta.disconnect()
    print("Aborting connection to AP!")
    disconnect_ct = nil
  end
end




function readout(temp)
  for addr, temp in pairs(temp) do
    print(string.format("Sensor %s: %s 'C", encoder.toHex(addr), temp))
    sendData(temp)
  end

  -- Module can be released when it is no longer needed
  t = nil
  package.loaded["ds18b20"]=nil
end

function goToSleep()
    print("Going to deep sleep for "..(time_between_sensor_readings/1000).." seconds")
    node.dsleep(time_between_sensor_readings*1000)
end

function sendData(temp)
  print("Posting data...")
  body = "{ \"sensorId\" : \"".. sensorId .."\", \"value\" : ".. temp .."  }"
  print("Body: " .. body)
  http.post("http://192.168.11.9:3000/values",
            " HTTP/1.1\r\n" ..
            "Host: 192.168.11.9:3000\r\n"..
            "Content-Type: application/json\r\n",
            body,
            httpCallback
          )
end

function httpCallback(code, data)
  if (code < 0) then
    print("HTTP request failed")
  else
    print(code,data)
  end
  goToSleep()
end

wifi.setmode(wifi.STATION)
wifi.sta.config({ssid=cfg.WifiSSID, pwd=cfg.WifiPassword, save=true})
if client_ip ~= "" then
    wifi.sta.setip({ip=client_ip,netmask=client_netmask,gateway=client_gateway})
end
-- Register WiFi Station event callbacks
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)
